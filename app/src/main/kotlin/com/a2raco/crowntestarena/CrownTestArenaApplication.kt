@file:Suppress("unused")

package com.a2raco.crowntestarena

import android.app.Application
import timber.log.Timber

class CrownTestArenaApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        // Plant Timber

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }
}
