plugins {
    id ("com.android.application")
    kotlin("android")
    id("de.nanogiants.android-versioning") version "2.4.0"
}

android {
    compileSdk = 31

    defaultConfig {
        applicationId = "com.a2raco.crowntestarena"
        minSdk = 23
        targetSdk = 31
        versionCode = versioning.getVersionCode()
        versionName = versioning.getVersionName()

        setProperty("archivesBaseName", "crowntestarena")
    }
    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            applicationIdSuffix = ".debug"
        }
        getByName("release"){
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-Xopt-in=kotlin.RequiresOptIn" + "-Xinline-classes"
    }
    buildFeatures {
        viewBinding = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.0.4"
    }
    lint {
        lintConfig=file("lint.xml")
    }
}

dependencies {
    implementation ("androidx.core:core-ktx:1.7.0")

    implementation ("androidx.appcompat:appcompat:1.4.0")
    implementation ("com.google.android.material:material:1.4.0")

    implementation ("androidx.compose.ui:ui:1.0.5")
    implementation ("androidx.compose.material:material:1.0.5")
    implementation ("androidx.compose.ui:ui-tooling:1.0.5")

    implementation ("androidx.constraintlayout:constraintlayout-compose:1.0.0-rc02")

    implementation ("androidx.activity:activity-compose:1.4.0")

    // logging
    implementation ("com.jakewharton.timber:timber:5.0.1")

    // debugImplementation ("com.squareup.leakcanary:leakcanary-android:2.7")
}
